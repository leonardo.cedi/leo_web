
import random
print("Juego de Ahorcado")
resp1=input(str("Elija modo de Juego Facil(1) / Dificil(2):  "))
cond1= "2" in resp1
intentosRestantes=3
palabras=["Capuchino","Gatito","Seguridad","Pollos","Programacion","Riachuelo","Infinito","Alentar","Político","Mascota","Esponja","Sombra","Claustrofobia","Beso","Cuatro"]
ind=random.randint(0,len(palabras)-1)
pal=palabras[ind]
pal=pal.upper()
letra0=pal[0]
letraU=pal[-1]
letra1=pal[random.randrange(2,len(pal)-1)]

def perder():
    print("Haz Perdido!")
    print("La Respuesta Correcta Era: {}".format(pal))
    quit()

def ganar():
    print("Haz Ganado!")
    quit()

def codigo():
    global intentosRestantes
    global letra0
    global letra1
    global letraU
    global pal

    posLetraU=pal.index(letraU)
    posLetra1=pal.index(letra1)
    pos1=pal.index(letra1)-1
    pos2=posLetraU-posLetra1-1

    print("Pista: "+letra0+("_"*pos1)+letra1+("_"*pos2)+letraU)
    if cond1==True: print("Intentos Restantes: {}".format(intentosRestantes))
    if intentosRestantes==0 and cond1==True: perder()
    resp2=input(str("Adivine La Palabra:")).upper()
    if resp2==pal:ganar()
    intentosRestantes=intentosRestantes-1
    codigo()

codigo()